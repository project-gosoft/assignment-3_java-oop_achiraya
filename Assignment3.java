import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class Assignment3 {

	static ArrayList<Integer> computer = new ArrayList<>();
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {

		// เพิ่มข้อมูล
		int[] ids = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		for (int id : ids) {
			computer.add(id);
		}

		System.out.println("Have all computers : " + computer);
		System.out.println("Number of computers added : " + ids.length + " machines");
		System.out.print("Please select the ID you want to delete : ");

		// รับค่า / ลบ
		int IdComputer = sc.nextInt();
		for (int i = 0; i <= computer.size(); i++) {
			if (IdComputer == i) {
				computer.remove(i - 1);
				System.out.println("Report");
				System.out.println("The deleted Id is : " + IdComputer);
				System.out.println("Number of computers available : " + computer.size() + " machines");
				System.out.println("Is the following id : " + computer);
			}
		}
	}
}
